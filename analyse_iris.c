#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "./headers/tab_funcs.h"
#include "./headers/data_funcs.h"

int main(int argn, char* argv[])
{
    /*
    * Programme déchiffrant et exploitant les données flottantes d'un fichier .dat
    * Il affiche la moyenne, le maximum et minimum par espèce d'Iris.
    */
    int descriptor;
    float *tab, ** iris;
    if(argn != 2) // Échec nb d'arguments
    {
        fprintf(stderr, "%s: un argument requis\n", argv[0]);
        exit(1);
    }
    else if(descriptor = open(argv[1], O_RDONLY) == -1) // Échec d'ouverture du fichier
    {
        fprintf(stderr, "%s: impossible d'ouvrir le fichier\n", argv[1]);
        exit(2);
    }
    else
    {
        tab = readAllFloats(descriptor); // Peut échouer pendant la lecture d'une donnée (EXIT_CODE = 2)
        iris = decompose(tab);
        free(tab); // On libère l'espace de tab, plus besoin de stocker après décomposition
        showResults(iris);
        freeTabs(iris);
        exit(0);
    }
}
