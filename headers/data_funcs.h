#include "../sources/data_funcs.c"

const int NB_VALUE = 150; // Nombre de valeurs flottantes dans le fichier

float moyDoubleTab(float*, int); // Fonction calculant la moyenne d'un tableau de flottant d'une taille donnée
float minTab(float*, int); // Fonction retournant le minimum d'un tableau de flottant d'une taille donnée
float maxTab(float*, int); // Fonction retournant le maximum d'un tableau de flottant d'une taille donnée