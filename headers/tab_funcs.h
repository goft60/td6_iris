#include "../sources/tab_funcs.c"

const int NB_TABS = 3; // Nombre de décomposition du tableau stockant les valeurs flottantes du fichier (on décompose en 3 ici)

float* readAllFloats(int); // Fonction retournant un tableau de flottant contenant les valeurs flottantes lues dans le fichier ouvert
float** decompose(float*); // Fonction retournant 3 tableaux de flottants décomposés suivant le tableau donné

void freeTabs(float**); // Libère l'espace mémoire occupé par le tableau 2D passé en paramètre
void showResults(float**); // Affiche les résultats sur les données de chaque espèce d'Iris