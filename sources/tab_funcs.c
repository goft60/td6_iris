
float* readAllFloats(int descriptor)
{
    /*
    * Fonction retournant le tableau stockant les valeurs flottantes d'un fichier ouvert
    * Inclus dans tab_funcs.h
    *   Exemple :
    *   int desc = open("toto.dat", O_RDONLY);
    *   float *values = readAllFloats(desc);
    */
    float *p, *tab;
    int val_read;
    p = tab =(float*) malloc(NB_VALUE * sizeof(float));
    while((val_read=read(descriptor, p++, sizeof(float))) != 0) // Lecture de la valeur de retour du read
        if(val_read == -1) // Échec de la lecture de donnée
        {
            fprintf(stderr, "Erreur pendant la lecture du fichier\n");
            exit(2);
        }
    return tab;
}

float** decompose(float* seq)
{
    /*
    * Fonction découpant un tableau 1D en NB_TABS de tableaux de longueur NB_VALUE/NB_TABS
    * Inclus dans tab_funcs.h
    *   Exemple :
        int desc = open("toto.dat", O_RDONLY);
        float** tabs = decompose(readAllFloats(desc));
    */
    float *p = seq, **pt, **iris;
    int length_tabs = NB_VALUE / NB_TABS;
    pt = iris =(float**) malloc(NB_TABS * sizeof(float*));
    for(int i = 0; i < NB_TABS; i++, pt++)
    {
        float *pt_float = *pt = (float*) malloc(length_tabs * sizeof(float)); // Allocation mémoire pour chaque "sous-tableau"
        for(int j = 0; j < length_tabs; j++, p++, pt_float++)
            *pt_float = *p;
    }
    return iris;
}
void freeTabs(float** tabs)
{
    /*
    * Fonction libérant la mémoire allouée d'un tableau 2D
    * Inclus dans tab_funcs.h
    *   Exemple :
        int desc = open("toto.dat", O_RDONLY);
        float** tabs = decompose(readAllFloats(desc));
        freeTabs(tabs);
    */
    float** pt = tabs;
    for(int i = 0; i < NB_TABS; i++, pt++)
        free(*pt);
    free(tabs);
}
void showResults(float**tabs)
{
    /*
    * Fonction affichant les résultats sur les données d'espèces d'Iris
    * Inclus dans tab_funcs.h
    *   Exemple :
        int desc = open("toto.dat", O_RDONLY);
        float** tabs = decompose(readAllFloats(desc));
        showResults(tabs);
    
    */
    char names[3][16] = { "Setosa", "Versicolor", "Virginica"};
    float** pt = tabs;
    int nb_values_table = NB_VALUE / NB_TABS;
    for(int i = 0; i < NB_TABS; i++, pt++)
    {
        printf("Espece Iris %s:\n", names[i]);
        printf("\tMoyenne : %f\n\tMax : %f\n\tMin : %f\n", moyDoubleTab(*pt, nb_values_table), maxTab(*pt, nb_values_table), minTab(*pt, nb_values_table)); // Fonctions inclues dans data_funcs.h
    }
}