float moyFloatTab(float* tab, int length)
{
    /*
    * Fonction retournant la moyenne d'un tableau de 'flottant' tab de taille 'length'
    * Inclus dans data_funcs.h
    *   Exemple :
    *   float values[3] = {2.0, 5.0, 9.0, 0.0};
    *   printf("Moyenne de values : %f", moyFloatTab(values, 4)); // Affiche ici 4.0
    */
    float *p = tab, sum = 0.0;
    for(int id_nb = 0; id_nb < length; id_nb++, p++)
        sum += *p;
    return sum / length;
}
float minTab(float* tab, int length)
{
    /*
    * Fonction retournant le minimum d'un tableau de 'flottant' tab de taille 'length'
    * Inclus dans data_funcs.h
    *   Exemple :
    *   float values[3] = {2.0, 5.0, 9.0};
    *   printf("Moyenne de values : %f", minTab(values, 3)); // Affiche ici 2.0
    */
    float* pt = tab;
    float min = *pt;
    for(int i = 0; i < length; i++, pt++)
        if(*pt < min)
            min = *pt;
    return min;
}
float maxTab(float* tab, int length)
{
    /*
    * Fonction retournant le maximum d'un tableau de 'flottant' tab de taille 'length'
    * Inclus dans data_funcs.h
    *    Exemple :
    *    float values[3] = {2.0, 5.0, 9.0};
    *    printf("Moyenne de values : %f", maxTab(values, 3)); // Affiche ici 9.0
    */
    float* pt = tab;
    float max = *pt;
    for(int i = 0; i < length; i++, pt++)
        if(*pt > max)
            max = *pt;
    return max;
}